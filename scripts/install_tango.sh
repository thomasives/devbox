#!/usr/bin/bash

mkdir -p build

pushd build

VERSION=1.15.0
OPTL_GITLAB=https://github.com/open-telemetry/opentelemetry-cpp
URL=${OPTL_GITLAB}/archive/refs/tags/v${VERSION}.tar.gz

curl -Lo opentelemetry-${VERSION}.tar.gz ${URL}

tar xaf opentelemetry-${VERSION}.tar.gz

cmake -Bopentelemetry-${VERSION}-build -Sopentelemetry-cpp-${VERSION} \
      -DBUILD_TESTING=OFF -DCMAKE_POSITION_INDEPENDENT_CODE=ON        \
      -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_RPATH=/usr/local/lib64   \
      -DWITH_ABSEIL=ON -DWITH_OTLP_GRPC=ON -DWITH_OTLP_HTTP=ON        \
      -DCMAKE_CXX_STANDARD=17

ninja -Copentelemetry-${VERSION}-build install

VERSION=10.0.0
TSD_GITLAB=https://gitlab.com/api/v4/projects/24125890/packages/generic/TangoSourceDistribution
URL="${TSD_GITLAB}/${VERSION}/tango-${VERSION}.tar.gz"

curl -Lo tango-${VERSION}.tar.gz ${URL}

tar xaf tango-${VERSION}.tar.gz

cmake -Btango-${VERSION}-build -Stango-${VERSION} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo             \
    -DBUILD_TESTING=OFF                           \
    -DTSD_DATABASE=ON                             \
    -DTSD_HTML_DOCUMENTATION=OFF                  \
    -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib64       \
    -Dtangoidl_ROOT=tango-${VERSION}/lib/idl

ninja -Ctango-${VERSION}-build

ninja -Ctango-${VERSION}-build install

popd

rm -rf build
