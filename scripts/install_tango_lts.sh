#!/usr/bin/bash

mkdir -p build

pushd build

export PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig

VERSION=4.2.5
OMNIORB_SOURCEFORGE=https://sourceforge.net/projects/omniorb/files/omniORB/
URL=${OMNIORB_SOURCEFORGE}/omniORB-${VERSION}/omniORB-${VERSION}.tar.bz2/download

curl -Lo omniORB-${VERSION}.tar.bz2 ${URL}

tar xaf omniORB-${VERSION}.tar.bz2

pushd omniORB-${VERSION}

./configure --libdir=/usr/local/lib64
make -j8
make install

popd

VERSION=9.3.6
TSD_GITLAB=https://gitlab.com/api/v4/projects/24125890/packages/generic/TangoSourceDistribution
URL="${TSD_GITLAB}/${VERSION}/tango-${VERSION}.tar.gz"

curl -Lo tango-${VERSION}.tar.gz ${URL}

tar xaf tango-${VERSION}.tar.gz

pushd tango-${VERSION}

./configure --libdir=/usr/local/lib64
make -j8
make install

popd

popd

rm -rf build
