ARG BASE=registry.gitlab.com/thomasives/devbox/base:f41-1
FROM ${BASE}

RUN curl -Lo /tmp/zig.tar.xz https://ziglang.org/download/0.13.0/zig-linux-x86_64-0.13.0.tar.xz && \
    tar -xaf /tmp/zig.tar.xz -C /opt/tri && \
    chmod +x /opt/tri/zig-linux-x86_64-0.13.0/zig && \
    rm -f /tmp/zig.tar.xz && ln -sf /opt/tri/zig-linux-x86_64-0.13.0/zig /usr/local/bin/zig && \
    curl -Lo /tmp/zls.tar.xz https://github.com/zigtools/zls/releases/download/0.13.0/zls-x86_64-linux.tar.xz && \
    mkdir /opt/tri/zls-linux-x86_64-0.13.0 && tar -xaf /tmp/zls.tar.xz -C /opt/tri/zls-linux-x86_64-0.13.0 && \
    chmod +x /opt/tri/zls-linux-x86_64-0.13.0/zls && \
    rm -f /tmp/zls.tar.xz && ln -sf /opt/tri/zls-linux-x86_64-0.13.0/zls /usr/local/bin/zls
