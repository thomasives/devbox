ARG BASE=registry.gitlab.com/thomasives/devbox/base:f41-1
FROM ${BASE}

RUN dnf install -y --setopt=install_weak_deps=False --best \
        ant                       \
        boost-devel               \
        catch-devel               \
        clang                     \
        clang-tools-extra         \
        cmake                     \
        cppzmq-devel              \
        doxygen                   \
        gdb                       \
        grpc-devel                \
        java-21-openjdk           \
        libcurl-devel             \
        libjpeg-devel             \
        mariadb-connector-c-devel \
        ninja-build               \
        omniORB-devel             \
        patch                     \
        pkgconfig                 \
        pre-commit                \
        python-sphinx_rtd_theme   \
        python3-devel             \
        python3-setuptools        \
        python3-sphinx            \
        python3-sphinx-copybutton \
        zeromq-devel              \
        zlib-static               \
    && dnf clean all &&           \
    rm -rf /var/cache/dnf

ENV CMAKE_EXPORT_COMPILE_COMMANDS=ON CMAKE_GENERATOR=Ninja

COPY scripts/install_tango.sh /opt/tri

RUN /opt/tri/install_tango.sh
