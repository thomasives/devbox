ARG BASE=registry.gitlab.com/thomasives/devbox/base:f41-1
FROM ${BASE}

RUN dnf install -y --setopt=install_weak_deps=False --best \
        boost-devel               \
        clang                     \
        clang-tools-extra         \
        cmake                     \
        cppzmq-devel              \
        doxygen                   \
        java-11-openjdk-devel     \
        libjpeg-devel             \
        mariadb-connector-c-devel \
        ninja-build               \
        pkgconfig                 \
        python3-devel             \
        python3-setuptools        \
        zeromq-devel              \
    && dnf clean all &&           \
    rm -rf /var/cache/dnf

ENV CMAKE_EXPORT_COMPILE_COMMANDS=ON CMAKE_GENERATOR=Ninja

COPY scripts/install_tango_lts.sh /opt/tri

RUN /opt/tri/install_tango_lts.sh
