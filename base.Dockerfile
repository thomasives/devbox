ARG BASE=registry.fedoraproject.org/fedora-toolbox:41
FROM ${BASE}

RUN dnf update -y &&                                       \
    dnf install -y --setopt=install_weak_deps=False --best \
        direnv                                             \
        fd-find                                            \
        fish                                               \
        fzf                                                \
        gcc                                                \
        gcc-c++                                            \
        git                                                \
        git-absorb                                         \
        glibc-langpack-en                                  \
        jq                                                 \
        make                                               \
        neovim                                             \
        pinentry                                           \
        ripgrep                                            \
        wl-clipboard                                       \
    && dnf clean all &&                                    \
    rm -rf /var/cache/dnf

RUN ln -sf /usr/share/fzf/shell/key-bindings.fish \
        /etc/fish/functions/fzf_key_bindings.fish

RUN mkdir -p /opt/tri && chmod 777 /opt/tri
